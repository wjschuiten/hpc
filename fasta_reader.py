#!/usr/bin/env python3
import mmap
import os
from itertools import islice
import time
import multiprocessing as mp
import argparse as ap
import sys
from multiprocessing.managers import BaseManager
from queue import Queue

def argparse():
    parser = ap.ArgumentParser()
    parser.add_argument("input_file", help="The fastq file you want to calculate phred scores from")
    parser.add_argument("-n", "--process_number", help = "The number of processes to run the code on", type = int, default = 1)
    parser.add_argument("-o", "--output_file", help = "Optional output file to write average scores to")
    args = parser.parse_args()
    if len(sys.argv) <2:
        parser.print_help(sys.stderr)
        sys.exit(1)
    return args

class fasta_reader():

    def __init__(self, file, processes):
        self.file = file
        self.size = os.stat(self.file).st_size
        self.chunks = processes
        self.chunk_size = int(self.size / self.chunks)

    def make_chunks(self):
        # Creates start values for your chunks
        batchindexes = []
        i = 0
        while i < self.size-self.chunk_size:
            batchindexes.append(i)
            i += self.chunk_size
        return batchindexes

    def read_chunk(self, start, file_obj):
        #  takes start value of chunk, and your file object
        file_obj.seek(start)
        file_obj.readline()  # Gotta skip a line to get to the phred score
        end = (start + self.chunk_size)
        if end > self.size:  # Value could be higher than file size due to taking next line from the bit
            end = self.size
        count = 3
        max = 0
        phreds = []
        while start < end:
            if count % 4 != 0:
                file_obj.readline()
                count += 1
            else:
                index = 0
                max += 1
                phred_line = file_obj.readline()
                for code in phred_line.encode('ascii'):
                    try:
                        phreds[index]+=(code-33)
                        index += 1
                    except IndexError:
                        phreds.append(code-33)
                        index += 1
                        continue
                count = 1
                start = file_obj.tell()
        phreds = [round(x/max, 2) for x in phreds]
        del phreds[-1]
        return phreds

    def read_fasta(self, start, q):
        # Input: start bit value of where your batch starts, queue object of multiprocessing module
        file_obj = open(self.file, 'r')
        file_obj.seek(start)
        first_line = file_obj.readline()
        # Search for read header
        while not first_line.startswith("@"):
            first_line = file_obj.readline()
        first_start = file_obj.tell()
        second_line = file_obj.readline()
        # @ could also be a phred score character, check that it isn't
        if second_line.startswith("@"):
            second_start = file_obj.tell()
            total_phreds = self.read_chunk(second_start, file_obj)
        else:
            total_phreds = self.read_chunk(first_start, file_obj)
        q.put(total_phreds)

def main():
    # Get args
    start = time.time()
    args = argparse()
    inputfile = args.input_file
    processes = args.process_number
    # Create fasta_reader object and create start values for chunks
    reader = fasta_reader(inputfile, processes)
    batches = reader.make_chunks()
    print(batches)
    # Init multiprocess variables
    q = mp.Queue()
    jobs = []
    result = []
    print("Ready to go, amount of batches made:", len(batches))
    for batch in batches:
        p = mp.Process(target=reader.read_fasta, args=(batch, q))
        jobs.append(p)
        p.start()
    for job in jobs:
        time.sleep(0.1)
        job.join()
    while q.empty() == False:
        result.append(q.get())
    print("Appended results, total batches run: ", len(result))
    stop = time.time()
    print("I finished in {} seconds".format(round(stop-start,2)))


if __name__ == "__main__":
    main()