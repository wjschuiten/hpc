#!/usr/bin/env python3

import mmap
import os
from itertools import islice
import time
import multiprocessing as mp
import argparse as ap
import sys
from multiprocessing.managers import BaseManager, SyncManager
from queue import Queue

def argparse():
    parser = ap.ArgumentParser()
    parser.add_argument("--client", help="Let the script know you are running the client instead of the master")
    args = parser.parse_args()
    return args

def read_chunk(start, file_obj, end, size):
    #  takes start value of chunk, and your file object
    file_obj.seek(start)
    file_obj.readline()  # Gotta skip a line to get to the phred score
    if end > size:  # Value could be higher than file size due to taking next line from the bit
        end = size
    count = 3
    max = 0
    phreds = []
    while start < end:
        if count % 4 != 0:
            file_obj.readline()
            count += 1
        else:
            index = 0
            max += 1
            phred_line = file_obj.readline()
            for code in phred_line.encode('ascii'):
                try:
                    phreds[index]+=(code-33)
                    index += 1
                except IndexError:
                    phreds.append(code-33)
                    index += 1
                    continue
            count = 1
            start = file_obj.tell()
    phreds = [round(x/max, 2) for x in phreds]
    del phreds[-1]
    return phreds

def read_fasta(file_obj,start, q, end, size):
    # Input: start bit value of where your batch starts, queue object of multiprocessing module
    file_obj.seek(start)
    first_line = file_obj.readline()
    # Search for read header
    while not first_line.startswith("@"):
        first_line = file_obj.readline()
    first_start = file_obj.tell()
    second_line = file_obj.readline()
    # @ could also be a phred score character, check that it isn't
    if second_line.startswith("@"):
        second_start = file_obj.tell()
        total_phreds = read_chunk(second_start, file_obj, end, size)
    else:
        total_phreds = read_chunk(first_start, file_obj, end, size)
        q.put(total_phreds)

def make_chunks(size, chunk_size):
    # Creates start values for your chunks
    batchindexes = []
    i = 0
    while i < size - chunk_size:
        batchindexes.append(int(i))
        i += chunk_size
    return batchindexes

def make_client_chunks(start, end, chunk_size):
    # Creates start values for your chunks
    batchindexes = []
    while start < end:
        batchindexes.append(int(start))
        start += chunk_size
    return batchindexes

def create_master(HOST, PORT, AUTHKEY):
    InputQueue = Queue()
    OutputQueue = Queue()

    #Hardcoded vars, to be removed
    file = "/commons/Themas/Thema12/HPC/rnaseq.fastq"
    processes = 100

    size = os.stat(file).st_size
    chunk_size = int(size / processes)
    batches = make_chunks(size, chunk_size)

    #Make a list containing the beginning bit and end bit for each process
    begin_end_positions = [[x, x + batches[1]] for x in batches if(x != batches[-1])]
    #Add to queue
    [InputQueue.put(x) for x in begin_end_positions]

    class QueueManager(SyncManager):
        pass

    QueueManager.register('get_InputQueue', callable=lambda: InputQueue)
    QueueManager.register('get_OutputQueue', callable=lambda: OutputQueue)
    manager = QueueManager(address = (HOST, PORT), authkey = AUTHKEY)
    manager.start() # This actually starts the server
    print("Made manager!")
    master = Manager('nuc111', 5294, b'letmein')
    master.run_master()




class Manager():

    def __init__(self, address, port, key):
        self.address = address
        self.port = port
        self.key = key

    def run_client(self):
        #While loop, try connect to master, send empty list when connected in queue
        class QueueManager(SyncManager):
            pass
        connected = False
        while not connected:
            try:
                m = QueueManager(address=(self.address, self.port), authkey=self.key)
                m.connect()
                print("Succesfully connected!")
                connected = True
            except ConnectionRefusedError:
                print("Connection failed, trying again in 10 seconds...")
                time.sleep(10)
                continue
        QueueManager.register('get_InputQueue')
        QueueManager.register('get_OutputQueue')

        #Sloppy hardcoding file and size again, tbf
        size = os.stat("/commons/Themas/Thema12/HPC/rnaseq.fastq").st_size


        InputQueue = m.get_InputQueue()
        OutputQueue = m.get_OutputQueue()
        while not InputQueue.empty():
            processes = 8 #all clients use 8 cores for now
            input = InputQueue.get()
            print(input)
            start = input[0]
            end = input[1]
            chunk_size = (end - start) / processes
            batches = make_client_chunks(start, end, chunk_size)
            # Init multiprocess variables
            q = mp.Queue()
            jobs = []
            result = []
            for batch in batches:
                file_obj = open("/commons/Themas/Thema12/HPC/rnaseq.fastq", 'r')
                end = batch + chunk_size
                p = mp.Process(target=read_fasta, args=(file_obj, batch, q, end, size))
                jobs.append(p)
                p.start()
            while q.empty() == False:
                result.append(q.get())
            OutputQueue.put(result)




    def run_master(self):
        #While loop, try connect to master, send empty list when connected in queue
        class QueueManager(SyncManager):
            pass
        connected = False
        while not connected:
            try:
                m = QueueManager(address=(self.address, self.port), authkey=self.key)
                m.connect()
                print("Succesfully connected master!")
                connected = True
            except ConnectionRefusedError:
                print("Connection failed, trying again in 10 seconds...")
                time.sleep(10)
                continue
        QueueManager.register('get_InputQueue')
        QueueManager.register('get_OutputQueue')
        running = True
        InputQueue = m.get_InputQueue()
        OutputQueue = m.get_OutputQueue()
        phreds = []
        max = 0
        start = time.time()
        while running:
            while not InputQueue.empty():
                print("Waiting on clients to finish")
                if OutputQueue.empty():
                    time.sleep(5.0)
                else:
                    while not OutputQueue.empty():
                        scores = OutputQueue.get()
                        max +=1
                        j = 0
                        for score in scores:                   # [[1,1,1], [1,1,1]]
                            for i in range(len(score)):        # 0,1,2
                                while j < len(score):                  # while j <= 3
                                    try:
                                        phreds[j] += scores[i][j]       #phreds 0 += first list first element
                                        j+=1
                                    except IndexError:
                                        phreds.append(scores[i][j])
                                        j+=1
            print("Everything is finished")
            print("Final result is: ", [round(x / max, 2) for x in phreds])
            print("I finished in {} seconds".format(round(time.time() - start, 2)))
            running = False
            # While stack not empty AND outputqueue is empty
            # Wait for client to put in outputqueue
            # Repeat till inputqueue is empty empty
            # Master may have to connect to worker to share queues, and keep a list of workers, investigate



def main():
    args = argparse()
    if args.client:
        # Maak clients aan voor elke worker
        worker = Manager('nuc111', 5294, b'letmein')
        try:
            worker.run_client()
        except EOFError:
            pass
        #worker.client_empty_queue()
    else:
        create_master("",5294,b'letmein')





if __name__ == "__main__":
    main()